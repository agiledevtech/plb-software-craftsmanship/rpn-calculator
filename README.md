# RPN Calculator

## Mise en oeuvre d'une calculatrice en Reverse Polish Notation

ex. : 

> 3 4 +

> 4 2 -

> 1 2 + 3 /

> 2 4 / 5 6 - *

> 3 5 6 + *

> 3 5 +

> 1 3 2 + *

Résultats :
> 7

> 2

> 1

> -0.5

> 33

> 8

>7 
