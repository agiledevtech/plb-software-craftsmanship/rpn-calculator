public class OperatorFactory {

    public static OperatorStrategy resolve(final String operator) {
        switch (operator) {
            case RPNCalculator.PLUS_OPERATOR -> {
                return new Addition();
            }
            case RPNCalculator.MULTIPLIER_OPERATOR -> {
                return new Multiplication();
            }
            case RPNCalculator.SUBSTRACT_OPERATOR -> {
                return new Soustraction();
            }
            default -> {
                return null;
            }
        }
    }
}
