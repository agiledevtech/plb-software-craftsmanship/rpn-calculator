public interface OperatorStrategy {

    int compute(int first, int second);
}
