public class Multiplication implements OperatorStrategy {
    @Override
    public int compute(int first, int second) {
        return first * second;
    }
}
