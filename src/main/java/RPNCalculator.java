import static java.lang.Integer.parseInt;

public class RPNCalculator {

    public static final int FIRST_ELEMENT_INDEX = 0;
    public static final int SECOND_ELEMENT_INDEX = 1;
    public static final int OPERATOR_INDEX = 2;
    public static final String PLUS_OPERATOR = "+";
    public static final String SEPARATOR = " ";
    public static final String MULTIPLIER_OPERATOR = "*";

    public static final String SUBSTRACT_OPERATOR = "-";

    public static int transform(String input) {
        String[] elements;
        try {
            return parseInt(input);
        } catch (NumberFormatException e) {
            elements = input.split(SEPARATOR);
            String operator = elements[OPERATOR_INDEX];

            return OperatorFactory.resolve(operator).compute(parseInt(elements[FIRST_ELEMENT_INDEX]),parseInt(elements[SECOND_ELEMENT_INDEX]));


        }
    }
}
