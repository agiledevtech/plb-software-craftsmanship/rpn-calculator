import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("RPNCalculator")
public class RPNCalculatorTest {

    @Test
    public void should_transform_1_for_numeral_One(){
        String input="1";
        int result=RPNCalculator.transform(input);
        Assertions.assertEquals(1,result);

    }
    @Test
    public void should_transform_2_for_numeral_Two(){
        String input="2";
        int result=RPNCalculator.transform(input);
        Assertions.assertEquals(2,result);

    }

    @Test
    @DisplayName("should transform '1 2 +' to 3")
    public void should_transform_sum_two_integers() {
        String input="1 2 +";
        int result = RPNCalculator.transform(input);
        Assertions.assertEquals(3, result);
    }

    @Test
    @DisplayName("should transform '4 2 +' to 6")
    public void test() {
        String input="4 2 +";
        int result = RPNCalculator.transform(input);
        Assertions.assertEquals(6, result);
    }

    @Test
    @DisplayName("should transform '4 2 -' to 2")
    public void test_soustraction() {
        String input="4 2 -";
        int result = RPNCalculator.transform(input);
        Assertions.assertEquals(2, result);
    }

    @Test
    @DisplayName("should transform '2 4 *' to 8")
    public void test_multiplication() {
        String input="2 4 *";
        int result = RPNCalculator.transform(input);
        Assertions.assertEquals(8, result);
    }




}
